[Adblock Plus 2.0]
! Title: .MGID Default filters
! Expires: 1 days
!:partner_token=MGID Inc.
@@||jsc.mgid.com^
@@||mgid.com^$popup
@@||servicer.mgid.com^$script
@@||s-img.mgid.com/g/$image,xmlhttprequest
@@||cdn.mgid.com/images/$image
@@||cdn.mgid.com/js/wglibs/$script
@@||cm.mgid.com^$script,image
@@||cm.lentainform.com^$image
@@||c.mgid.com^$image
@@||mgid.com/ghits^$xmlhttprequest
@@||c.marketgid.com^$image
@@||autocounter.lentainform.com^$image
@@||jsc.marketgid.com^
#@#div[id*="ScriptRoot"]
##div[id*="ScriptRoot"]:not([class*="eyeo"])
#@#div[id*="MarketGid"]
#@#div[id^="M"][id*="Composite"]
#@#a[href^="//www.mgid.com/"]
#@#.mgline

! Idealmedia
@@||jsc.idealmedia.io^$script
@@||idealmedia.io^$script,image,xmlhttprequest,popup,stylesheet,other
@@||s-img.idealmedia.io^$image,xmlhttprequest
@@||cdn.idealmedia.io^$image,script
@@||autocounter.idealmedia.io^$image,xmlhttprequest
@@||servicer.idealmedia.io^$script,xmlhttprequest
@@||mgid.com^$popup,third-party
@@||idealmedia.io^$popup
#@#a[href*="idealmedia.io"]
#@#.mgline
##div[id*="ScriptRoot"]:not([class*="eyeo"])
#@#div[id*="ScriptRoot"]