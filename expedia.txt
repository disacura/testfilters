[Adblock Plus 2.0]
! Title: .Expedia Default filters
! Expires: 1 days
!:partner_token=Expedia
@@||uciservice.com/assets/meso-loaders/px.gif?ch=1&rn=*$image
||uciservice.com/assets/meso-loaders/px.gif?ch=2&rn=*$image
@@||uciservice.com/assets/meso-loaders/expads-blocked.js
@@||td.doubleclick.net/td/rul/*?random=*$subdocument
@@||googlesyndication.com/safeframe/1-0-40/html/container.html$document,subdocument
@@||securepubads.g.doubleclick.net/gampad/ads^$xmlhttprequest
@@||securepubads.g.doubleclick.net/pagead/managed/js/gpt/*/pubads_impl.js^
@@||securepubads.g.doubleclick.net/tag/js/gpt.js
@@||tpc.googlesyndication.com/simgad^$image
@@||tpc.googlesyndication.com/sodar^
@@||google.com/pagead/form-data^
@@||pagead2.googlesyndication.com/getconfig/sodar^
#@#[id^="google_ads_iframe"]
#@#div[data-google-query-id]
#@#.sponsored-text
#@#.NativeAdContainerRegion
#@#[data-stid="meso-similar-properties-carousel"]
#@#[data-meso-viewed]
#@#a[href^="https://adclick.g.doubleclick.net/"]
