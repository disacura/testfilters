[Adblock Plus 2.0]
! Title: .Adpushup/Adrecover Default filters
! Expires: 1 days
!:partner_token=AdPushup
@@||delivery.adrecover.com^$document,subdocument
@@||delivery.adrecover.com^*/adRecover.js
@@||feedback.adrecover.com^
@@||delivery.adrecover.com/allow.jpg
||delivery.adrecover.com/block.jpg
@@||g.doubleclick.net/pcs/click?$popup
@@||g.doubleclick.net/pcs/view?$image
@@||g.doubleclick.net/gampad/ads?$script,xmlhttprequest
@@||pagead2.googlesyndication.com/pagead/$script
@@||g.doubleclick.net/pagead/ads^*client=ca-pub-8655001720710332^$subdocument,document
@@||g.doubleclick.net/pagead/ads^*client=ca-pub-3191894791526522^$subdocument,document
@@||googlesyndication.com/simgad/$image
@@||googlesyndication.com/pagead/js/*/client/ext/m_window_focus_non_hydra.js
@@||googlesyndication.com/pagead/js/*/abg_lite.js
@@||adservice.google.*/adsid/integrator.js
#@#._ap_adrecover_ad
#@#.img_ad
